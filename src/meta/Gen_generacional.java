/**
 * @author Manuel Alférez Ruiz
 * @note Metaheurística Práctica 2
 */

package meta;

import java.util.Random;

public class Gen_generacional {
    // Constantes
    static final int LIBRE = -2;
    static final int ESTA = -1;

    // poblacion: Conjunto de individuos que conforman la población
    // TAM_GEN: Tamaño de la solución del problema (el tamaño del genotipo)
    // TAM_POBLACION: Tamaño del conjunto de individuos (tamaño de la población)
    Individuo[] poblacion;
    private static int TAM_GEN;
    private static int TAM_POBLACION = 50;

    private Fabrica fabrica;
    private Random random;
    private Individuo[] descendientes; // Hijos: serán cruzados y mutados
    private int[] padres_seleccionados; // Vector con las posiciones de los padres seleccionados
    private int num_padres_seleccionados;
    private int posPadre, posMadre; // Padre y Madre seleccionados en el torneo

    /**
     * @brief Genera la población inicial de manera aleatoria
     */
    private void inicializar_poblacion() {
        // Unidades de montaje para insertar aleatoriamente
        int[] posiciones = new int[TAM_GEN];
        for (int i = 0; i < TAM_POBLACION; i++) poblacion[i] = new Individuo(TAM_GEN);
        int tama_logico;
        int posAleatoria;
        for (int i = 0; i < TAM_POBLACION; i++) {
            tama_logico = TAM_GEN;
            for (int k = 0; k < TAM_GEN; k++) posiciones[k] = k;
            for (int j = 0; j < TAM_GEN; j++) {
                posAleatoria = random.nextInt(tama_logico);
                poblacion[i].genotipo[j] = posiciones[posAleatoria];
                posiciones[posAleatoria] = posiciones[tama_logico - 1];
                tama_logico--;
            } // for j
        } // for i
    } // inicializar_poblacion()

    /**
     * @brief Calcula el fitness de la población inicial
     */
    private void calcular_fitness_inicial() {
        for (int k = 0; k < TAM_POBLACION; k++) poblacion[k].calcular_fitness(fabrica);
    } // calcular_fitness_inicial() -> Complejidad = n³

    /**
     * @brief Operador de selección
     * @post Selecciona de acuerdo a las directrices marcadas por un torneo binario, escogiendo el de mejor fitness,
     * repitiendo el proceso hasta haber seleccionado un conjunto de individuos tan grande como la población
     */
    private void seleccion() {
        padres_seleccionados = new int[TAM_POBLACION];
        num_padres_seleccionados = 0;
        do {
            posPadre = random.nextInt(TAM_POBLACION);
            do {
                posMadre = random.nextInt(TAM_POBLACION);
            } while (posMadre == posPadre);

            // Seleccionamos aquel individuo con mejor fitness
            if (poblacion[posPadre].fitness < poblacion[posMadre].fitness) {
                padres_seleccionados[num_padres_seleccionados++] = posPadre;
            } else {
                padres_seleccionados[num_padres_seleccionados++] = posMadre;
            }
        } while (num_padres_seleccionados < TAM_POBLACION);
    } // seleccion()

    /**
     * @brief Operador de cruce (cruce de orden)
     * @post Cruzar los dos padres padres consecutivos (si se da la probabilidad de cruce) y calcular dos hijos
     */
    private void cruce_orden() {
        // Posición de la madre y del padre (Padre = rango_cruce, Madre = rango_cruce + 1
        int rango_cruce = 0;
        double aleatorio; // Para saber si realizar cruce o no
        double P_cruce = 0.7;
        do {
            aleatorio = random.nextDouble();
            posPadre = padres_seleccionados[rango_cruce];
            posMadre = padres_seleccionados[rango_cruce + 1];
            // Se realiza cruce
            if (aleatorio <= P_cruce) {
                int posA = random.nextInt(TAM_GEN);
                int posB = random.nextInt(TAM_GEN);
                int c;
                int num_h1 = 0, num_h2 = 0;
                // posA tiene que ser menor que posB
                if (posA > posB) {
                    c = posA;
                    posA = posB;
                    posB = c;
                }
                // Creación e inicialización de los hijos
                descendientes[rango_cruce] = new Individuo(TAM_GEN);
                descendientes[rango_cruce + 1] = new Individuo(TAM_GEN);

                // Indicación de que los descendientes se han cruzado
                descendientes[rango_cruce].set_cruzado();
                descendientes[rango_cruce + 1].set_cruzado();

                int[] alelos_hijo1 = new int[TAM_GEN];
                int[] alelos_hijo2 = new int[TAM_GEN];
                for (int i = 0; i < TAM_GEN; i++) {
                    alelos_hijo1[i] = i; // Para comprobar que no se repiten al copiar al hijo
                    alelos_hijo2[i] = i;
                }

                // Copia en los hijos los alelos comprendidos entre posA y posB
                for (int i = posA; i <= posB; i++) {
                    // Primer hijo
                    descendientes[rango_cruce].genotipo[i] = poblacion[posPadre].genotipo[i];
                    alelos_hijo1[poblacion[posPadre].genotipo[i]] = ESTA;
                    num_h1++;
                    // Segundo hijo
                    descendientes[rango_cruce + 1].genotipo[i] = poblacion[posMadre].genotipo[i];
                    alelos_hijo2[poblacion[posMadre].genotipo[i]] = ESTA;
                    num_h2++;
                }

                // it: Iterador
                // k: Posición donde insertar en el hijo1
                // z: Posición donde insertar en el hijo2
                int it, k, z;

                // Si el segmento está al final
                if (posB + 1 == TAM_GEN) {
                    it = k = z = 0;
                } else {
                    it = k = z = posB + 1;
                }
                if (it != posA) {
                    do {
                        // Primer hijo
                        if (alelos_hijo1[poblacion[posMadre].genotipo[it]] != ESTA) {
                            descendientes[rango_cruce].genotipo[k] = poblacion[posMadre].genotipo[it];
                            alelos_hijo1[poblacion[posMadre].genotipo[it]] = ESTA;
                            k++;
                            num_h1++;
                        }
                        // Segundo hijo
                        if (alelos_hijo2[poblacion[posPadre].genotipo[it]] != ESTA) {
                            descendientes[rango_cruce + 1].genotipo[z] = poblacion[posPadre].genotipo[it];
                            alelos_hijo2[poblacion[posPadre].genotipo[it]] = ESTA;
                            z++;
                            num_h2++;
                        }
                        it++;
                        if (k == TAM_GEN) k = 0;
                        if (z == TAM_GEN) z = 0;
                        if (it == TAM_GEN) it = 0;
                    } while (num_h1 < TAM_GEN || num_h2 < TAM_GEN);
                }
            } else { // No se realiza cruce
                // Creación e inicialización de los hijos
                descendientes[rango_cruce] = new Individuo(poblacion[posPadre]);
                descendientes[rango_cruce + 1] = new Individuo(poblacion[posMadre]);
            }
            rango_cruce += 2;
        } while (rango_cruce != TAM_POBLACION);
    } // cruce_orden()

    /**
     * @brief Operador de cruce (cruce PMX)
     * @post Cruzar los dos padres padres consecutivos (si se da la probabilidad de cruce) y calcular dos hijos
     */
    private void cruce_PMX() {

        // Posición de la madre y del padre (Padre = rango_cruce, Madre = = rango_cruce + 1
        int rango_cruce = 0;
        double aleatorio; // Para saber si realizar cruce o no
        double P_cruce = 0.7;
        do {

            aleatorio = random.nextDouble();
            posPadre = padres_seleccionados[rango_cruce];
            posMadre = padres_seleccionados[rango_cruce + 1];

            // Se realiza cruce
            if (aleatorio <= P_cruce) {

                int posA = random.nextInt(TAM_GEN);
                int posB;
                do {
                    posB = random.nextInt(TAM_GEN);
                } while (posA == posB);
                int c;
                int num_h1 = 0, num_h2 = 0;

                // posA tiene que ser menor que posB
                if (posA > posB) {
                    c = posA;
                    posA = posB;
                    posB = c;
                }
                // Creación e inicialización de los hijos
                descendientes[rango_cruce] = new Individuo(TAM_GEN);
                descendientes[rango_cruce + 1] = new Individuo(TAM_GEN);

                // Indicación de que los descendientes se han cruzado
                descendientes[rango_cruce].set_cruzado();
                descendientes[rango_cruce + 1].set_cruzado();

                int[] alelos_hijo1 = new int[TAM_GEN];
                int[] alelos_hijo2 = new int[TAM_GEN];
                for (int i = 0; i < TAM_GEN; i++) {
                    descendientes[rango_cruce].genotipo[i] = LIBRE; // Para indicar una posición libre en el hijo
                    descendientes[rango_cruce + 1].genotipo[i] = LIBRE;
                    alelos_hijo1[i] = i; // Para comprobar que no se repiten al copiar al hijo
                    alelos_hijo2[i] = i;
                }

                // Copia en los hijos los alelos comprendidos entre posA y posB
                for (int i = posA; i <= posB; i++) {
                    // Primer hijo
                    descendientes[rango_cruce].genotipo[i] = poblacion[posPadre].genotipo[i];
                    alelos_hijo1[poblacion[posPadre].genotipo[i]] = ESTA;
                    num_h1++;
                    // Segundo hijo
                    descendientes[rango_cruce + 1].genotipo[i] = poblacion[posMadre].genotipo[i];
                    alelos_hijo2[poblacion[posMadre].genotipo[i]] = ESTA;
                    num_h2++;
                }

                // Iterador
                int it = posA;
                int it2; // Usado para pivotar
                boolean parar;
                do {
                    it2 = it;
                    parar = false;

                    // Comprobación si el valor (de la madre) no esta en el hijo1
                    if (alelos_hijo1[poblacion[posMadre].genotipo[it]] != -1) {
                        // Mientras no se encuentre posición vacía en el hijo (con -2)
                        while (!parar) {
                            if (descendientes[rango_cruce].genotipo[it2] == LIBRE) {
                                descendientes[rango_cruce].genotipo[it2] = poblacion[posMadre].genotipo[it];
                                alelos_hijo1[poblacion[posMadre].genotipo[it]] = ESTA;
                                num_h1++;
                                parar = true;
                            } else {
                                it2 = poblacion[posMadre].busca_valor(descendientes[rango_cruce].genotipo[it2]);
                            }
                        } // while
                    } // if

                    it2 = it;
                    parar = false;
                    // Comprobación si el valor (del padre) no esta en el hijo2
                    if (alelos_hijo2[poblacion[posPadre].genotipo[it]] != ESTA) {
                        // Mientras no se encuentre posición vacía en el hijo (con -2)
                        while (!parar) {
                            if (descendientes[rango_cruce + 1].genotipo[it2] == LIBRE) {
                                descendientes[rango_cruce + 1].genotipo[it2] = poblacion[posPadre].genotipo[it];
                                alelos_hijo2[poblacion[posPadre].genotipo[it]] = ESTA;
                                num_h2++;
                                parar = true;
                            } else {
                                it2 = poblacion[posPadre].busca_valor(descendientes[rango_cruce + 1].genotipo[it2]);
                            }
                        } // while
                    } // if

                    it++;
                    if (it == TAM_GEN) it = 0;
                } while (num_h1 < TAM_GEN || num_h2 < TAM_GEN);

            } else { // No se realiza cruce
                // Creación e inicialización de los hijos
                descendientes[rango_cruce] = new Individuo(poblacion[posPadre]);
                descendientes[rango_cruce + 1] = new Individuo(poblacion[posMadre]);
            }
            rango_cruce += 2;
        } while (rango_cruce != TAM_POBLACION);
    } // cruce_PMX()

    /**
     * @brief Operador de mutación
     * @post Muta cada alelo de cada hijo de acuerdo a una probabilidad
     * @note Si el hijo no ha sido cruzado, entonces se realiza el calculo de su fitness después de cada mutación
     */
    private void mutacion() {
        double aleatorio; // Para saber si mutar o no el alelo
        int pos_aleatoria;
        // Probabilidad de mutación
        double P_mutacion = 0.001 * TAM_GEN;
        int aux; // Variable auxiliar para intercambio
        int turno_individuo = 0;

        do {
            // Si se ha cruzado el descendiente el fitness se calculará después
            if (descendientes[turno_individuo].ha_cruzado()) {
                for (int i = 0; i < TAM_GEN; i++) {
                    aleatorio = random.nextDouble();
                    if (aleatorio <= P_mutacion) {
                        pos_aleatoria = random.nextInt(TAM_GEN);
                        aux = descendientes[turno_individuo].genotipo[i];
                        descendientes[turno_individuo].genotipo[i] = descendientes[turno_individuo].genotipo[pos_aleatoria];
                        descendientes[turno_individuo].genotipo[pos_aleatoria] = aux;
                    }
                } // for i
            } else { // Si no se ha cruzado el individuo el fitness se calcula en cada permutación
                for (int i = 0; i < TAM_GEN; i++) {
                    aleatorio = random.nextDouble();
                    if (aleatorio <= P_mutacion) {
                        pos_aleatoria = random.nextInt(TAM_GEN);
                        descendientes[turno_individuo].calcular_fitness_parcial(fabrica, i, pos_aleatoria);
                    }
                } // for i
                Main.evaluaciones++;
            }
            turno_individuo++;
        } while (turno_individuo < TAM_POBLACION);
    } // mutacion()

    /**
     * @brief Evaluación de los dos nuevos individuos
     */
    private void evaluacion() {
        for (int i = 0; i < TAM_POBLACION; i++) {
            if (descendientes[i].ha_cruzado()) {
                descendientes[i].calcular_fitness(fabrica);
            }
        }
    } // evaluacion()

    /**
     * @brief Reemplazo de individuos
     * @post Reemplaza la población vieja por la nueva, con élite de un individuo de la antigua
     */
    private void reemplazamiento() {
        // Búsqueda del elite y del peor de los descendientes
        int pos_elite = 0;
        int pos_peor_descendiente = 0;
        // Búsqueda de los peores dos individuos de la población
        for (int i = 1; i < TAM_POBLACION; i++) {
            if (poblacion[pos_elite].fitness > poblacion[i].fitness) {
                pos_elite = i;
            }
            if (descendientes[pos_peor_descendiente].fitness < descendientes[i].fitness) {
                pos_peor_descendiente = i;
            }
        }
        // Log      -inicio-
        Main.informe_G += "ELITE:\n";
        Main.informe_G += +poblacion[pos_elite].id;
        Main.informe_G += " ; ";
        for (int i = 0; i < fabrica.num_unidades; i++)
            Main.informe_G = Main.informe_G + " " + poblacion[pos_elite].genotipo[i];
        Main.informe_G += " ; " + poblacion[pos_elite].fitness;
        Main.informe_G += " ; " + poblacion[pos_elite].generacion;
        Main.informe_G += " ;<->; ";
        // Log      -fin-

        // Log      -inicio-
        Main.informe_G += +descendientes[pos_peor_descendiente].id;
        Main.informe_G += " ; ";
        for (int i = 0; i < fabrica.num_unidades; i++)
            Main.informe_G = Main.informe_G + " " + descendientes[pos_peor_descendiente].genotipo[i];
        Main.informe_G += " ; " + descendientes[pos_peor_descendiente].fitness;
        Main.informe_G += " ; " + descendientes[pos_peor_descendiente].generacion;
        Main.informe_G += "\n";
        // Log      -fin-

        // El elite pasa a la nueva población
        descendientes[pos_peor_descendiente].copiar_individuo(poblacion[pos_elite]);

        // Sustitución de los padres por la nueva población
        for (int i = 0; i < TAM_POBLACION; i++) {
            poblacion[i].copiar_individuo(descendientes[i]);
        }
    } // reemplazamiento()

    /**
     * @param fabrica Fabrica en la que aplicar el algoritmo
     * @brief Algoritmo principal de la clase
     */
    void algoritmo_principal(Fabrica fabrica) {
        // Semilla inicializada
        random = new Random(Main.semilla);
        Main.evaluaciones = 0;
        Main.generaciones = 0;
        Individuo.NUM = 0;

        this.fabrica = fabrica;
        TAM_GEN = fabrica.num_unidades;

        // Creación de la matriz con los 50 individuos
        poblacion = new Individuo[TAM_POBLACION];
        descendientes = new Individuo[TAM_POBLACION];
        // Inicialización de las poblaciones
        inicializar_poblacion();

        // Cálculo del fitness de la población
        calcular_fitness_inicial();

        do {
            Main.generaciones++;
            seleccion();
            if (Main.Opcion_local.equals(Main.CRUCE_ORDEN) || Main.Opcion_local.equals(Main.ALL)) {
                cruce_orden();
            }
            if (Main.Opcion_local.equals(Main.CRUCE_PMX) || Main.Opcion_local.equals(Main.ALL)) {
                cruce_PMX();
            }
            mutacion();
            evaluacion();
            reemplazamiento();
        } while (Main.evaluaciones < 50000);
        Main.generaciones++;
    } // algoritmo_principal()
}
