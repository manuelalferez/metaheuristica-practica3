/**
 * @author Manuel Alférez Ruiz
 * @note Metaheurística Práctica 2
 */

package meta;

// Dependencies

import java.io.*;
import java.util.Date;

/**
 * @brief Función principal
 */
public class Main {

    // Constantes globales
    static final String CRUCE_ORDEN = "ORDEN";
    static final String CRUCE_PMX = "PMX";
    static final String ALL = "ALL";

    static final String diez = "10";
    static final String tres = "3";

    // Variables locales
    //  Semillas
    private static int[] semillas = new int[5];
    private static final String SEPARATOR = ",";
    private static String[] tipos_algoritmos = new String[8];
    //  Almacena los nombres de los dos tipos de enfriamientos
    private static String[] nombres_Tipo = new String[2];
    //  Almacena los nombres de los 10 archivos de datos
    private static String[] nombreArchivo = new String[10];

    // Almacena las distintas iteraciones de la BL de los meméticos
    private static int[] val_it = new int[2];
    private static String[] val_num = new String[3];

    // Variables globales
    public static int generaciones; //Número de generaciones que se han generado
    public static int evaluaciones;

    public static int val_ite;
    private static String val_ite_param;
    public static String val_num_ind;
    public static String val_num_ind_param;

    //  Enfriamiento Simulado:
    //      1=Geométrico
    //      2=Boltzmann
    public static int tipo;

    public static String informe_E = "ID;Genotipo;Fitness;Generacion;Intercambiado por;ID;Genotipo;Fitness;Generacion\n";
    public static String informe_G = "ID;Genotipo;Fitness;Generacion;Intercambiado por;ID;Genotipo;Fitness;Generacion\n";
    public static String informe_sol_E = "ID;Genotipo;Fitness;Generacion\n";
    public static String informe_sol_G = "ID;Genotipo;Fitness;Generacion\n";
    public static String informe_AME = "ID;Genotipo;Fitness;Generacion;Intercambiado por;ID;Genotipo;Fitness;Generacion\n";
    public static String informe_AMG = "ID;Genotipo;Fitness;Generacion;Intercambiado por;ID;Genotipo;Fitness;Generacion\n";
    public static String informe_sol_AME = "ID;Genotipo;Fitness;Generacion\n";
    public static String informe_sol_AMG = "ID;Genotipo;Fitness;Generacion\n";
    public static String informe_Enfriamiento_Simulado;
    //  Semilla escogida
    public static int semilla;
    //  Opciones
    static String OPCION_CRUCE;
    static String Opcion_local;
    public static String OPCION_algoritmo_seleccionado;
    public static String OPCION_archivo_seleccionado;
    public static String OPCION_semilla;

    /**
     * @param mensaje Mensaje a escribir en el fichero
     * @brief Método que escribe en un fichero
     * @post Escribir un mensaje en un fichero llamado solucion.txt
     */
    public static void escribirFichero(String nombre_Archivo, String mensaje) {
        FileWriter fichero = null;
        PrintWriter pw;
        try {
            fichero = new FileWriter(nombre_Archivo, true);
            pw = new PrintWriter(fichero);
            pw.println(mensaje);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Para asegurarnos que se cierra el fichero
                if (null != fichero)
                    fichero.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    } // escribirFichero()

    /**
     * @param est          El algoritmo estacionario
     * @param num_unidades Número de unidades
     * @param tipo         Tipo de algoritmo estacionario (CRUCE ORDEN O CRUCE PMX)
     * @return Devuelve la posición del mejor individuo
     * @brief Escribe en el informe el resultado para el Algoritmo genético estacionario:
     * - Primero graba toda la población (mientras que haya el mejor individuo)
     * - Después graba quién es el mejor individuo
     * - Finalmente, graba las evaluaciones y las generaciones totales
     */
    public static int escribir_solucion_estacionario(Gen_estacionario est, int num_unidades, String tipo) {
        int posMejor = 0;
        informe_sol_E += "POBLACION ESTACIONARIO " + tipo + ":\n";
        for (int i = 0; i < 50; i++) {
            informe_sol_E += est.poblacion[i].id + ";";
            for (int j = 0; j < num_unidades; j++)
                informe_sol_E += " " + est.poblacion[i].genotipo[j];
            informe_sol_E += "; Fitness: " + est.poblacion[i].fitness;
            informe_sol_E += "; Generacion: " + est.poblacion[i].generacion;
            informe_sol_E += "\n";
            if (est.poblacion[posMejor].fitness > est.poblacion[i].fitness)
                posMejor = i;
        }
        informe_sol_E += "EL MEJOR: INDIVIDUO " + posMejor + " ID: " + est.poblacion[posMejor].id + ", FITNESS: " +
                est.poblacion[posMejor].fitness;
        informe_sol_E += "\nEvaluaciones: " + Main.evaluaciones + "  |  Generaciones: " + Main.generaciones;
        informe_sol_E += "\n\n";

        return posMejor;
    } // escribir_solucion_estacionario()

    /**
     * @param gene         El algoritmo generacional
     * @param num_unidades Número de unidades
     * @param tipo         Tipo de algoritmo estacionario (CRUCE ORDEN O CRUCE PMX)
     * @return Devuelve la posición del mejor individuo
     * @brief Escribe en el informe el resultado para el Algoritmo genético generacional:
     * - Primero graba toda la población (mientras que haya el mejor individuo)
     * - Después graba quién es el mejor individuo
     * - Finalmente, graba las evaluaciones y las generaciones totales
     */
    public static int escribir_solucion_generacional(Gen_generacional gene, int num_unidades, String tipo) {
        int posMejor = 0;
        informe_sol_G += "POBLACION GENERACIONAL " + tipo + ":\n";
        for (int i = 0; i < 50; i++) {
            informe_sol_G += gene.poblacion[i].id + ";";
            for (int j = 0; j < num_unidades; j++)
                informe_sol_G += " " + gene.poblacion[i].genotipo[j];
            informe_sol_G += "; Fitness: " + gene.poblacion[i].fitness;
            informe_sol_G += "; Generacion: " + gene.poblacion[i].generacion;
            informe_sol_G += "\n";
            if (gene.poblacion[posMejor].fitness > gene.poblacion[i].fitness)
                posMejor = i;
        }
        informe_sol_G += "EL MEJOR: INDIVIDUO " + posMejor + " ID: " + gene.poblacion[posMejor].id + ", FITNESS: " +
                gene.poblacion[posMejor].fitness;
        informe_sol_G += "\nEvaluaciones: " + Main.evaluaciones + "  |  Generaciones: " + Main.generaciones;
        informe_sol_G += "\n\n";
        return posMejor;
    } // escribir_solucion_generacional()

    /**
     * @param ame          El algoritmo AME
     * @param num_unidades Número de unidades
     * @return Devuelve la posición del mejor individuo
     * @brief Escribe en el informe el resultado para el Algoritmo AME:
     * - Primero graba toda la población (mientras que haya el mejor individuo)
     * - Después graba quién es el mejor individuo
     * - Finalmente, graba las evaluaciones y las generaciones totales
     */
    public static int escribir_solucion_ame(AME ame, int num_unidades) {
        int posMejor = 0;
        informe_sol_AME += "POBLACION AME :\n";
        for (int i = 0; i < 25; i++) {
            informe_sol_AME += ame.poblacion[i].id + ";";
            for (int j = 0; j < num_unidades; j++)
                informe_sol_AME += " " + ame.poblacion[i].genotipo[j];
            informe_sol_AME += "; Fitness: " + ame.poblacion[i].fitness;
            informe_sol_AME += "; Generacion: " + ame.poblacion[i].generacion;
            informe_sol_AME += "\n";
            if (ame.poblacion[posMejor].fitness > ame.poblacion[i].fitness)
                posMejor = i;
        }
        informe_sol_AME += "EL MEJOR: INDIVIDUO " + posMejor + " ID: " + ame.poblacion[posMejor].id + ", FITNESS: " +
                ame.poblacion[posMejor].fitness;
        informe_sol_AME += "\nEvaluaciones: " + Main.evaluaciones + "  |  Generaciones: " + Main.generaciones;
        informe_sol_AME += "\n\n";
        return posMejor;
    } // escribir_solucion_ame()

    /**
     * @param amg          El algoritmo AMG
     * @param num_unidades Número de unidades
     * @return Devuelve la posición del mejor individuo
     * @brief Escribe en el informe el resultado para el Algoritmo AMG:
     * - Primero graba toda la población (mientras que haya el mejor individuo)
     * - Después graba quién es el mejor individuo
     * - Finalmente, graba las evaluaciones y las generaciones totales
     */
    public static int escribir_solucion_amg(AMG amg, int num_unidades) {
        int posMejor = 0;
        informe_sol_AMG += "POBLACION AMG :\n";
        for (int i = 0; i < 10; i++) {
            informe_sol_AMG += amg.poblacion[i].id + ";";
            for (int j = 0; j < num_unidades; j++)
                informe_sol_AMG += " " + amg.poblacion[i].genotipo[j];
            informe_sol_AMG += "; Fitness: " + amg.poblacion[i].fitness;
            informe_sol_AMG += "; Generacion: " + amg.poblacion[i].generacion;
            informe_sol_AMG += "\n";
            if (amg.poblacion[posMejor].fitness > amg.poblacion[i].fitness)
                posMejor = i;
        }
        informe_sol_AMG += "EL MEJOR: INDIVIDUO " + posMejor + " ID: " + amg.poblacion[posMejor].id + ", FITNESS: " +
                amg.poblacion[posMejor].fitness;
        informe_sol_AMG += "\nEvaluaciones: " + Main.evaluaciones + "  |  Generaciones: " + Main.generaciones;
        informe_sol_AMG += "\n\n";
        return posMejor;
    } // escribir_solucion_amg()

    /**
     * @throws IOException Excepción en caso de error
     * @brief Lectura de los parámetros del fichero "param.csv"
     * @post Asignar a cada variable los campos leídos del archivo de parámetros
     */
    public static void lectura_de_parametros() throws IOException {
        // Abro el .csv en buffer de lectura
        BufferedReader bufferLectura = new BufferedReader(new FileReader("param.csv"));
        String linea = bufferLectura.readLine(); // Leo una línea del archivo
        // Lectura del tipo de algoritmo a ejecutar
        String[] campos = linea.split(String.valueOf(SEPARATOR));
        OPCION_algoritmo_seleccionado = campos[1].toUpperCase();
        linea = bufferLectura.readLine(); // Vuelvo a leer del fichero
        // Lectura de la semilla
        campos = linea.split(String.valueOf(SEPARATOR));
        OPCION_semilla = campos[1].toUpperCase();
        linea = bufferLectura.readLine();
        // Lectura del archivo seleccionado
        campos = linea.split(String.valueOf(SEPARATOR));
        OPCION_archivo_seleccionado = campos[1];
        linea = bufferLectura.readLine();
        // Lectura del tipo de cruce a usar en los genéticos
        campos = linea.split(String.valueOf(SEPARATOR));
        OPCION_CRUCE = campos[1].toUpperCase();
        linea = bufferLectura.readLine();
        // Lectura del número de individuos en la BL de los meméticos
        campos = linea.split(String.valueOf(SEPARATOR));
        val_num_ind_param = campos[1].toUpperCase();
        linea = bufferLectura.readLine();
        // Lectura del número de iteraciones de la BL en los meméticos
        campos = linea.split(String.valueOf(SEPARATOR));
        //val_ite = Integer.parseInt(campos[1]);
        val_ite_param = campos[1].toUpperCase();
        System.out.printf("Fichero de parámetros leído exitosamente.\n");

        // Cierro el buffer de lectura
        if (bufferLectura != null) {
            bufferLectura.close();
        }
    } // lectura_de_parametros()

    /**
     * @param solucion Vector con la solución del problema
     * @param fabrica  Fabrica con las matrices de piezas transferidas y las distancias entre unidades de producción
     * @return El peso de la solucion
     * @brief Calcula el peso de la solución
     */
    private static int calcularPeso(int[] solucion, Fabrica fabrica) {
        int peso = 0;
        for (int i = 0; i < solucion.length; i++)
            for (int j = 0; j < solucion.length; j++)
                if (i != j)
                    peso += fabrica.piezas[i][j] * fabrica.distancias[solucion[i]][solucion[j]];

        return peso;
    } // calcularPeso()

    /**
     * @param args Argumentos
     * @brief Función principal
     */
    public static void main(String[] args) {
        /*------------------VARIABLES------------------*/
        // Asignación de archivos y semillas a los vectores
        nombreArchivo[0] = "cnf01.dat";
        nombreArchivo[1] = "cnf02.dat";
        nombreArchivo[2] = "cnf03.dat";
        nombreArchivo[3] = "cnf04.dat";
        nombreArchivo[4] = "cnf05.dat";
        nombreArchivo[5] = "cnf06.dat";
        nombreArchivo[6] = "cnf07.dat";
        nombreArchivo[7] = "cnf08.dat";
        nombreArchivo[8] = "cnf09.dat";
        nombreArchivo[9] = "cnf10.dat";

        int semilla0 = 53911043;
        semillas[0] = semilla0;
        int semilla1 = 12345678;
        semillas[1] = semilla1;
        int semilla2 = 34567812;
        semillas[2] = semilla2;
        int semilla3 = 45678123;
        semillas[3] = semilla3;
        int semilla4 = 56781234;
        semillas[4] = semilla4;

        // Declaración de variables
        String direccion;
        Fabrica fabrica;
        nombres_Tipo[0] = "Geométrico";
        nombres_Tipo[1] = "Boltzmann";

        tipos_algoritmos[0] = "GREEDY";
        tipos_algoritmos[1] = "BL";
        tipos_algoritmos[2] = "ES";
        tipos_algoritmos[3] = "AGE";
        tipos_algoritmos[4] = "AGG";
        tipos_algoritmos[5] = "AME";
        tipos_algoritmos[6] = "AMG";
        tipos_algoritmos[7] = "MEME";

        val_it[0] = 100;
        val_it[1] = 500;

        val_num[0] = tres;
        val_num[1] = diez;
        val_num[2] = ALL;

        Suma suma;
        Greedy greedy;
        BusquedaLocal primerMejor;
        EnfriamientoSimulado enfriamientoS;

        int[] solucion; // Vector donde se irán almacenando las soluciones de cada algoritmo
        long T; // El tiempo que ha tardado en ejecutarse un algoritmo
        int iteracion_ES, pesoSolucion, pos_Mejor, semilla_Actual = 0, pos_archivoActual = 0;

        long T_INICIO, T_FIN; //Variables para determinar el tiempo de ejecución

        boolean archivo_correcto = false, cruce_correcto = false, fin = false, fin2 = false;
        String archivoActual = "";

        // Para diferenciar las distintas ejecuciones con una fecha al inicio de cada archivo
        Date fecha = new Date();
        String mensaje = "************************************************************************************\n";
        mensaje += fecha.toString() + "\n"; // Texto con las solución que será introducido en el archivo solucion.txt
        mensaje += "************************************************************************************\n";

        informe_G += "************************************************************************************\n";
        informe_G += fecha.toString() + "\n";
        informe_G += "************************************************************************************\n";

        informe_E += "************************************************************************************\n";
        informe_E += fecha.toString() + "\n";
        informe_E += "************************************************************************************\n";

        informe_sol_E += "************************************************************************************\n";
        informe_sol_E += fecha.toString() + "\n";
        informe_sol_E += "************************************************************************************\n";

        informe_sol_G += "************************************************************************************\n";
        informe_sol_G += fecha.toString() + "\n";
        informe_sol_G += "************************************************************************************\n";

        informe_Enfriamiento_Simulado += "************************************************************************************\n";
        informe_Enfriamiento_Simulado += fecha.toString() + "\n";
        informe_Enfriamiento_Simulado += "************************************************************************************\n";

        informe_AME += "************************************************************************************\n";
        informe_AME += fecha.toString() + "\n";
        informe_AME += "************************************************************************************\n";

        informe_AMG += "************************************************************************************\n";
        informe_AMG += fecha.toString() + "\n";
        informe_AMG += "************************************************************************************\n";

        informe_sol_AMG += "************************************************************************************\n";
        informe_sol_AMG += fecha.toString() + "\n";
        informe_sol_AMG += "************************************************************************************\n";

        informe_sol_AME += "************************************************************************************\n";
        informe_sol_AME += fecha.toString() + "\n";
        informe_sol_AME += "************************************************************************************\n";

        /*------------------------------------------------------*/

        // Lectura de los parámetros del usuario
        try {
            lectura_de_parametros();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Búsqueda si el archivo introducido coincide con los disponibles
        for (int i = 0; i < 10; i++) {
            if (nombreArchivo[i].equals(OPCION_archivo_seleccionado) || OPCION_archivo_seleccionado.toUpperCase().equals(ALL)) {
                archivo_correcto = true;
                break;
            }
        }

        // Comprobación si el archivo existe
        if (archivo_correcto) {

            do {

                if (OPCION_archivo_seleccionado.toUpperCase().equals(ALL)) {
                    archivoActual = nombreArchivo[pos_archivoActual];
                } else {
                    archivoActual = OPCION_archivo_seleccionado;
                }

                // Creación de la fábrica y de la dirección del archivo elegido
                direccion = "datos/" + archivoActual;
                fabrica = new Fabrica(direccion);

                // Cálculo de la suma de Piezas y suma de Distancias
                suma = new Suma(fabrica);

                // Si se desea probar con todas las semillas
                if (OPCION_semilla.equals(ALL)) {
                    semilla_Actual = 0;
                }

                do {

                    if (!OPCION_semilla.equals(ALL)) {
                        semilla = Integer.parseInt(OPCION_semilla);
                    } else {
                        semilla = semillas[semilla_Actual];
                    }

                    mensaje = mensaje + "Archivo: " + archivoActual + " con Semilla: " + semilla + "\n";
                    informe_E += "\nArchivo: " + archivoActual + " con Semilla: " + semilla + "\n";
                    informe_G += "\nArchivo: " + archivoActual + " con Semilla: " + semilla + "\n";
                    informe_sol_G += "\nArchivo: " + archivoActual + " con Semilla: " + semilla + "\n";
                    informe_sol_E += "\nArchivo: " + archivoActual + " con Semilla: " + semilla + "\n";
                    informe_AMG += "\nArchivo: " + archivoActual + " con Semilla: " + semilla + " (" + val_num_ind + "-" + val_ite + ")  \n";
                    informe_AME += "\nArchivo: " + archivoActual + " con Semilla: " + semilla + "(2-" + val_ite + ")\n";
                    mensaje = mensaje + "Suma piezas: \n";
                    for (int i = 0; i < fabrica.num_unidades; i++) {
                        mensaje = mensaje + suma.sumaPiezas[i] + " ";
                    }

                    mensaje = mensaje + "\n\nSuma distancias: \n";
                    for (int i = 0; i < fabrica.num_unidades; i++) {
                        mensaje = mensaje + suma.sumaDistancias[i] + " ";
                    }

                    System.out.printf("\nArchivo: " + archivoActual + " con Semilla: " + semilla + "\n");

                    /*******************************************GREEDY******************************************/
                    if (OPCION_algoritmo_seleccionado.equals(tipos_algoritmos[0]) || OPCION_algoritmo_seleccionado.equals(ALL)) {
                        // Cálculo de la solución Greedy
                        greedy = new Greedy();
                        T_INICIO = System.currentTimeMillis();
                        solucion = greedy.algoritmoGreedy(suma);
                        T_FIN = System.currentTimeMillis();
                        T = T_FIN - T_INICIO;

                        mensaje = mensaje + "\n\nSolución Greedy: \n";
                        for (int i = 0; i < fabrica.num_unidades; i++) {
                            mensaje += solucion[i] + " ";
                        }
                        pesoSolucion = calcularPeso(solucion, fabrica);
                        mensaje += "\nCoste Greedy:                             " + pesoSolucion + "   Tiempo: " + T + " ms\n";

                        System.out.printf("Algoritmo Greedy finalizado.\n");
                    }

                    /*******************************************BL******************************************/
                    if (OPCION_algoritmo_seleccionado.equals(tipos_algoritmos[1]) || OPCION_algoritmo_seleccionado.equals(ALL)) {
                        // Cálculo de la solución de Búsqueda local Primer el mejor
                        primerMejor = new BusquedaLocal();
                        T_INICIO = System.currentTimeMillis();
                        solucion = primerMejor.busquedaLocalPrimerMejor(fabrica);
                        T_FIN = System.currentTimeMillis();
                        T = T_FIN - T_INICIO;

                        mensaje = mensaje + "Solución Búsqueda local primer mejor: \n";
                        for (int i = 0; i < fabrica.num_unidades; i++) {
                            mensaje = mensaje + solucion[i] + " ";
                        }
                        pesoSolucion = calcularPeso(solucion, fabrica);

                        mensaje += "Coste Primer Mejor:                       " + pesoSolucion + "    Tiempo: " + T + " ms\n";

                        System.out.printf("Algoritmo BL finalizado.\n");
                    }

                    /*******************************************ES******************************************/
                    if (OPCION_algoritmo_seleccionado.equals(tipos_algoritmos[2]) || OPCION_algoritmo_seleccionado.equals(ALL)) {
                        // Cálculo de la solución de la Búsqueda Local con Enfriamiento Simulado (Geométrico)
                        tipo = 1;
                        informe_Enfriamiento_Simulado = "Trayectoria de costes en cada iteración (" + nombres_Tipo[Main.tipo - 1] + "): \n";
                        enfriamientoS = new EnfriamientoSimulado();
                        T_INICIO = System.currentTimeMillis();
                        solucion = enfriamientoS.enfriamiento_Simulado(fabrica);
                        T_FIN = System.currentTimeMillis();
                        T = T_FIN - T_INICIO;
                        iteracion_ES = enfriamientoS.iteracion;

                        // Impresión de la solución de la Búsqueda Local con Enfriamiento Simulado
                        mensaje = mensaje + "\n\nSolución Enfriamiento Simulado (Geométrico): \n";
                        for (int i = 0; i < fabrica.num_unidades; i++) {
                            mensaje = mensaje + solucion[i] + " ";
                        }

                        pesoSolucion = calcularPeso(solucion, fabrica);

                        mensaje += "\nCoste Enfriamiento Simulado (" + nombres_Tipo[0] + "):  " + pesoSolucion + "    Tiempo: " + T + " ms\n" +
                                "     -Iteración: " + iteracion_ES + "\n";

                        // Cálculo de la solución de la Búsqueda Local con Enfriamiento Simulado (Boltzmann)
                        tipo = 2;
                        informe_Enfriamiento_Simulado = informe_Enfriamiento_Simulado + "Trayectoria de costes en cada iteración" +
                                " (" + nombres_Tipo[Main.tipo - 1] + "): \n";
                        enfriamientoS = new EnfriamientoSimulado();
                        T_INICIO = System.currentTimeMillis();
                        solucion = enfriamientoS.enfriamiento_Simulado(fabrica);
                        T_FIN = System.currentTimeMillis();
                        T = T_FIN - T_INICIO;
                        iteracion_ES = enfriamientoS.iteracion;

                        // Impresión de la solución de la Búsqueda Local con Enfriamiento Boltzmann
                        mensaje = mensaje + "Solución Enfriamiento Simulado (Boltzmann): \n";
                        for (int i = 0; i < fabrica.num_unidades; i++) {
                            mensaje = mensaje + solucion[i] + " ";
                        }
                        pesoSolucion = calcularPeso(solucion, fabrica);

                        mensaje += "Coste Enfriamiento Simulado (" + nombres_Tipo[1] + "):  " + pesoSolucion + "    Tiempo: " + T + " ms\n" +
                                "     -Iteración: " + iteracion_ES + "\n";

                        System.out.printf("Algoritmo ES finalizado.\n");

                    }

                    // Comprobación si el parámetro pasado para la opción del cruce es correcto
                    if (OPCION_CRUCE.equals(ALL) || OPCION_CRUCE.equals(CRUCE_PMX) || OPCION_CRUCE.equals(CRUCE_ORDEN)) {
                        cruce_correcto = true;
                    }
                    if (cruce_correcto) {
                        /*******************************************AGE ORDEN******************************************/
                        if (OPCION_algoritmo_seleccionado.equals(tipos_algoritmos[3]) || OPCION_algoritmo_seleccionado.equals(ALL)) {
                            if (OPCION_CRUCE.equals(CRUCE_ORDEN) || OPCION_CRUCE.equals(ALL)) {
                                // Cálculo Algoritmo Genético estacionario por cruce de Orden
                                Opcion_local = CRUCE_ORDEN;
                                Gen_estacionario estacionario_ORDEN = new Gen_estacionario();
                                informe_E = informe_E + "Solucion Algoritmo estacionario CRUCE ORDEN: \n";
                                T_INICIO = System.currentTimeMillis();
                                estacionario_ORDEN.algoritmo_principal(fabrica);
                                T_FIN = System.currentTimeMillis();
                                T = T_FIN - T_INICIO;

                                // Escritura en disco del log
                                Main.escribirFichero("logs/informe_E[" + archivoActual + "].csv", informe_E);
                                informe_E = "";

                                pos_Mejor = escribir_solucion_estacionario(estacionario_ORDEN, fabrica.num_unidades, "CRUCE ORDEN");

                                mensaje = mensaje + "\n\nSolucion Algoritmo estacionario CRUCE ORDEN (EL MEJOR): \n";
                                for (int i = 0; i < fabrica.num_unidades; i++) {
                                    mensaje += estacionario_ORDEN.poblacion[pos_Mejor].genotipo[i] + " ";
                                }
                                mensaje += "\nINDIVIDUO [" + pos_Mejor + "] ID: " + estacionario_ORDEN.poblacion[pos_Mejor].id +
                                        ", FITNESS: " + estacionario_ORDEN.poblacion[pos_Mejor].fitness +
                                        ", Generacion: " + estacionario_ORDEN.poblacion[pos_Mejor].generacion;
                                mensaje += "\nEvaluaciones: " + Main.evaluaciones + "  |  Generaciones: " + Main.generaciones +
                                        "   Tiempo: " + T + " ms\n";

                                System.out.printf("Algoritmo AGE ORDEN finalizado.\n");

                            }

                            /*******************************************AGE PMX******************************************/
                            if (OPCION_CRUCE.equals(CRUCE_PMX) || OPCION_CRUCE.equals(ALL)) {
                                Opcion_local = CRUCE_PMX;
                                // Cálculo Algoritmo Genético estacionario por cruce PMX
                                Gen_estacionario estacionario_PMX = new Gen_estacionario();
                                informe_E = informe_E + "Solucion Algoritmo estacionario CRUCE PMX: \n";
                                T_INICIO = System.currentTimeMillis();
                                estacionario_PMX.algoritmo_principal(fabrica);
                                T_FIN = System.currentTimeMillis();
                                T = T_FIN - T_INICIO;

                                // Escritura en disco del log
                                Main.escribirFichero("logs/informe_E[" + archivoActual + "].csv", informe_E);
                                informe_E = "";

                                pos_Mejor = escribir_solucion_estacionario(estacionario_PMX, fabrica.num_unidades, "CRUCE PMX");

                                mensaje = mensaje + "Solucion Algoritmo estacionario CRUCE PMX (EL MEJOR): \n";
                                for (int i = 0; i < fabrica.num_unidades; i++) {
                                    mensaje += estacionario_PMX.poblacion[pos_Mejor].genotipo[i] + " ";
                                }

                                mensaje += "\nINDIVIDUO [" + pos_Mejor + "] ID: " + estacionario_PMX.poblacion[pos_Mejor].id +
                                        ", FITNESS: " + estacionario_PMX.poblacion[pos_Mejor].fitness +
                                        ", Generacion: " + estacionario_PMX.poblacion[pos_Mejor].generacion;
                                mensaje += "\nEvaluaciones: " + Main.evaluaciones + "  |  Generaciones: " + Main.generaciones +
                                        "   Tiempo: " + T + " ms\n";

                                System.out.printf("Algoritmo AGE PMX finalizado.\n");
                            }
                        }

                        /*******************************************AGG ORDEN******************************************/
                        if (OPCION_algoritmo_seleccionado.equals(tipos_algoritmos[4]) || OPCION_algoritmo_seleccionado.equals(ALL)) {
                            if (OPCION_CRUCE.equals(CRUCE_ORDEN) || OPCION_CRUCE.equals(ALL)) {
                                Opcion_local = CRUCE_ORDEN;
                                // Cálculo Algoritmo Genético generacional por cruce de Orden
                                Gen_generacional generacional_ORDEN = new Gen_generacional();
                                informe_G = informe_G + "\n\nSolucion Algoritmo generacional CRUCE ORDEN: \n";
                                T_INICIO = System.currentTimeMillis();
                                generacional_ORDEN.algoritmo_principal(fabrica);
                                T_FIN = System.currentTimeMillis();
                                T = T_FIN - T_INICIO;

                                // Escritura en disco del log
                                Main.escribirFichero("logs/informe_G[" + archivoActual + "].csv", informe_G);
                                informe_G = "";

                                pos_Mejor = escribir_solucion_generacional(generacional_ORDEN, fabrica.num_unidades, "CRUCE ORDEN");

                                mensaje = mensaje + "Solucion Algoritmo generacional CRUCE ORDEN (EL MEJOR): \n";
                                for (int i = 0; i < fabrica.num_unidades; i++) {
                                    mensaje += generacional_ORDEN.poblacion[pos_Mejor].genotipo[i] + " ";
                                }

                                mensaje += "\nINDIVIDUO [" + pos_Mejor + "] ID: " + generacional_ORDEN.poblacion[pos_Mejor].id +
                                        ", FITNESS: " + generacional_ORDEN.poblacion[pos_Mejor].fitness +
                                        ", Generacion: " + generacional_ORDEN.poblacion[pos_Mejor].generacion;
                                mensaje += "\nEvaluaciones: " + Main.evaluaciones + "  |  Generaciones: " + Main.generaciones +
                                        "   Tiempo: " + T + " ms\n";

                                System.out.printf("Algoritmo AGG ORDEN finalizado.\n");

                            }

                            /*******************************************AGG PMX******************************************/
                            if (OPCION_CRUCE.equals(CRUCE_PMX) || OPCION_CRUCE.equals(ALL)) {
                                Opcion_local = CRUCE_PMX;
                                // Cálculo Algoritmo Genético generacional por cruce PMX
                                Gen_generacional generacional_PMX = new Gen_generacional();
                                informe_G = informe_G + "\n\nSolucion Algoritmo generacional CRUCE PMX: \n";
                                T_INICIO = System.currentTimeMillis();
                                generacional_PMX.algoritmo_principal(fabrica);
                                T_FIN = System.currentTimeMillis();
                                T = T_FIN - T_INICIO;

                                // Escritura en disco del log
                                Main.escribirFichero("logs/informe_G[" + archivoActual + "].csv", informe_G);
                                informe_G = "";

                                pos_Mejor = escribir_solucion_generacional(generacional_PMX, fabrica.num_unidades, "CRUCE PMX");

                                mensaje = mensaje + "Solucion Algoritmo generacional CRUCE PMX (EL MEJOR): \n";
                                for (int i = 0; i < fabrica.num_unidades; i++) {
                                    mensaje += generacional_PMX.poblacion[pos_Mejor].genotipo[i] + " ";
                                }

                                mensaje += "\nINDIVIDUO [" + pos_Mejor + "] ID: " + generacional_PMX.poblacion[pos_Mejor].id +
                                        ", FITNESS: " + generacional_PMX.poblacion[pos_Mejor].fitness +
                                        ", Generacion: " + generacional_PMX.poblacion[pos_Mejor].generacion;
                                mensaje += "\nEvaluaciones: " + Main.evaluaciones + "  |  Generaciones: " + Main.generaciones +
                                        "   Tiempo: " + T + " ms\n";

                                System.out.printf("Algoritmo AGG PMX finalizado.\n");
                            }
                        }

                        /******************************************* AME ******************************************/
                        if (OPCION_algoritmo_seleccionado.equals(tipos_algoritmos[5]) || OPCION_algoritmo_seleccionado.equals(ALL)
                                || OPCION_algoritmo_seleccionado.equals(tipos_algoritmos[7])) {
                            boolean fin_AME = false;
                            int it_actual = 0;
                            do {
                                if (val_ite_param.equals(ALL)) {
                                    val_ite = val_it[it_actual];
                                } else {
                                    val_ite = Integer.parseInt(val_ite_param);
                                }
                                // Cálculo Algoritmo AME
                                AME ame = new AME();
                                informe_AME = informe_AME + "\n\nSolucion Algoritmo AME (2-" + val_ite + "): \n";
                                T_INICIO = System.currentTimeMillis();
                                ame.algoritmo_principal(fabrica);
                                T_FIN = System.currentTimeMillis();
                                T = T_FIN - T_INICIO;

                                // Escritura en disco del log
                                Main.escribirFichero("logs/informe_AME[" + archivoActual + "].csv", informe_AME);
                                informe_AME = "";

                                pos_Mejor = escribir_solucion_ame(ame, fabrica.num_unidades);

                                mensaje = mensaje + "\n\nSolucion Algoritmo AME (EL MEJOR) (2-" + val_ite + "): \n";
                                for (int i = 0; i < fabrica.num_unidades; i++) {
                                    mensaje += ame.poblacion[pos_Mejor].genotipo[i] + " ";
                                }

                                mensaje += "\nINDIVIDUO [" + pos_Mejor + "] ID: " + ame.poblacion[pos_Mejor].id +
                                        ", FITNESS: " + ame.poblacion[pos_Mejor].fitness +
                                        ", Generacion: " + ame.poblacion[pos_Mejor].generacion;
                                mensaje += "\nEvaluaciones: " + Main.evaluaciones + "  |  Generaciones: " + Main.generaciones +
                                        "   Tiempo: " + T + " ms\n";

                                System.out.printf("Algoritmo AME finalizado. (2-" + val_ite + ")\n");
                                if (val_ite_param.equals(ALL)) {
                                    it_actual++;
                                    if (it_actual==2)
                                        fin_AME = true;

                                } else {
                                    fin_AME = true;
                                }
                            } while (!fin_AME);
                        }

                        /******************************************* AMG ******************************************/
                        if (OPCION_algoritmo_seleccionado.equals(tipos_algoritmos[6]) || OPCION_algoritmo_seleccionado.equals(ALL)
                                || OPCION_algoritmo_seleccionado.equals(tipos_algoritmos[7])) {
                            boolean fin_AMG = false, fin_AMG2=false;
                            int it_actual = 0;
                            int num_ind_actual=0;

                            do {
                                if (val_num_ind_param.equals(ALL)) {
                                    val_num_ind = val_num[num_ind_actual];
                                } else {
                                    val_num_ind = val_num_ind_param;
                                }
                                do {
                                    if (val_ite_param.equals(ALL)) {
                                        val_ite = val_it[it_actual];
                                    } else {
                                        val_ite = Integer.parseInt(val_ite_param);
                                    }
                                    // Cálculo Algoritmo AME
                                    AMG amg = new AMG();
                                    informe_AMG = informe_AMG + "\n\nSolucion Algoritmo AMG (" + val_num_ind + "-" + val_ite + "): \n";
                                    T_INICIO = System.currentTimeMillis();
                                    amg.algoritmo_principal(fabrica);
                                    T_FIN = System.currentTimeMillis();
                                    T = T_FIN - T_INICIO;

                                    // Escritura en disco del log
//                            Main.escribirFichero("logs/informe_AMG[" + archivoActual + "].csv", informe_AMG);
                                    informe_AMG = "";

                                    pos_Mejor = escribir_solucion_amg(amg, fabrica.num_unidades);

                                    mensaje = mensaje + "\nSolucion Algoritmo AMG (EL MEJOR) (" + val_num_ind + "-" + val_ite + "): \n";
                                    for (int i = 0; i < fabrica.num_unidades; i++) {
                                        mensaje += amg.poblacion[pos_Mejor].genotipo[i] + " ";
                                    }

                                    mensaje += "\nINDIVIDUO [" + pos_Mejor + "] ID: " + amg.poblacion[pos_Mejor].id +
                                            ", FITNESS: " + amg.poblacion[pos_Mejor].fitness +
                                            ", Generacion: " + amg.poblacion[pos_Mejor].generacion;
                                    mensaje += "\nEvaluaciones: " + Main.evaluaciones + "  |  Generaciones: " + Main.generaciones +
                                            "   Tiempo: " + T + " ms\n";

                                    System.out.printf("Algoritmo AMG finalizado. (" + val_num_ind + "-" + val_ite + ")\n");
                                    if (val_ite_param.equals(ALL)) {
                                        it_actual++;
                                        if (it_actual==2)
                                            fin_AMG = true;
                                    } else {
                                        fin_AMG = true;
                                    }
                                } while (!fin_AMG);
                                if (val_num_ind_param.equals(ALL)) {
                                    num_ind_actual++;
                                    if (num_ind_actual== 3 ) {
                                        fin_AMG2 = true;
                                    }
                                } else {
                                    fin_AMG2 = true;
                                }
                                it_actual=0;
                                fin_AMG=false;
                            }while(!fin_AMG2);
                        }

                    } else {
                        System.out.printf("----------------¡CRUCE INCORRECTO!----------------");
                        System.out.printf(" Por favor, verifique el cruce seleccionado en los parámetros.\n");
                        System.out.printf("El cruce seleccionado debe ser ORDEN o PMX, o ALL si desea ambos\n");
                    }
                    mensaje = mensaje + "\n**********************************************\n\n";
                    if (!OPCION_semilla.equals(ALL)) fin = true;
                    else semilla_Actual++;
                    if (semilla_Actual == 5) fin = true;

                    escribirFichero("solucion.txt", mensaje);
//                    Main.escribirFichero("logs/enfriamiento_simulado[" + archivoActual + "].log", informe_Enfriamiento_Simulado);
//                    Main.escribirFichero("logs/informe_sol_E[" + archivoActual + "].csv", informe_sol_E);
//                    Main.escribirFichero("logs/informe_sol_G[" + archivoActual + "].csv", informe_sol_G);
//                    Main.escribirFichero("logs/informe_sol_AME[" + archivoActual + "].csv", informe_sol_AME);
//                    Main.escribirFichero("logs/informe_sol_AMG[" + archivoActual + "].csv", informe_sol_AMG);
                    mensaje = "";
                    informe_Enfriamiento_Simulado = "";
                    informe_sol_G = "";
                    informe_sol_E = "";

                } while (!fin);

                if (!OPCION_archivo_seleccionado.toUpperCase().equals(ALL)) fin2 = true;
                else {
                    pos_archivoActual++;
                    semilla_Actual = 0;
                    fin = false;
                }
                if (pos_archivoActual == 10) fin2 = true;

            } while (!fin2);


            /*------------------ESCRITURAS EN FICHEROS------------------*/

            System.out.printf("Hecho. En el archivo soluciones.txt tiene sus soluciones.\n");
        } else { // Archivo introducido incorrecto
            System.out.printf("----------------¡ARCHIVO INCORRECTO!----------------\n");
            System.out.printf(" Por favor, verifique el archivo seleccionado en los parámetros.\n");
            System.out.printf("El archivo seleccionado debe estar entre cnf01.dat y cnf10.dat\n");
        }
    } // main()
} // class Main
