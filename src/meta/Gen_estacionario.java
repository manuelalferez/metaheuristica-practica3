/**
 * @author Manuel Alférez Ruiz
 * @note Metaheurística Práctica 2
 */

package meta;

// Dependencies

import java.util.Random;

/**
 * Clase que implementa la resolución del problema mediante un enfoque de Algoritmo genético Estacionario
 */
class Gen_estacionario {
    // Constantes
    static final int LIBRE = -2;
    static final int ESTA = -1;

    // poblacion: Conjunto de individuos que conforman la población
    // TAM_GEN: Tamaño de la solución del problema (el tamaño del genotipo)
    // TAM_POBLACION: Tamaño del conjunto de individuos (tamaño de la población)
    Individuo[] poblacion;
    private static int TAM_GEN;
    private static int TAM_POBLACION = 50;

    private Fabrica fabrica;
    private Random random;
    private Individuo hijo1, hijo2; // Hijos: serán cruzados y mutados
    private int posPadre, posMadre; // Padre y Madre seleccionados en el torneo

    /**
     * @brief Genera la población inicial de manera aleatoria
     */
    private void inicializar_poblacion() {
        // Unidades de montaje para insertar aleatoriamente
        int[] posiciones = new int[TAM_GEN];
        for (int i = 0; i < TAM_POBLACION; i++) poblacion[i] = new Individuo(TAM_GEN);
        int tama_logico;
        int posAleatoria;
        for (int i = 0; i < TAM_POBLACION; i++) {
            tama_logico = TAM_GEN;
            for (int k = 0; k < TAM_GEN; k++) posiciones[k] = k;
            for (int j = 0; j < TAM_GEN; j++) {
                posAleatoria = random.nextInt(tama_logico);
                poblacion[i].genotipo[j] = posiciones[posAleatoria];
                posiciones[posAleatoria] = posiciones[tama_logico - 1];
                tama_logico--;
            } // for j
        } // for i
    } // inicializar_poblacion()

    /**
     * @brief Calcula el fitness de la población inicial
     */
    private void calcular_fitness_inicial() {
        for (int k = 0; k < TAM_POBLACION; k++) poblacion[k].calcular_fitness(fabrica);
    } // calcular_fitness_inicial() -> Complejidad = n³

    /**
     * @brief Función selección
     * @post Selecciona dos padres de la población (distintos)
     */
    private void seleccion() {
        posPadre = random.nextInt(TAM_POBLACION);
        do {
            posMadre = random.nextInt(TAM_POBLACION);
        } while (posMadre == posPadre);
    } // seleccion()

    /**
     * @brief Operador de cruce (cruce de orden)
     * @post Cruzar los dos padres seleccionados previamente y calcular dos hijos
     */
    private void cruce_orden() {
        int posA = random.nextInt(TAM_GEN);
        int posB;
        do {
            posB = random.nextInt(TAM_GEN);
        } while (posA == posB);
        int num_h1 = 0, num_h2 = 0, c;

        // posA tiene que ser menor que posB
        if (posA > posB) {
            c = posA;
            posA = posB;
            posB = c;
        }
        // Creación e inicialización de los hijos
        hijo1 = new Individuo(TAM_GEN);
        hijo2 = new Individuo(TAM_GEN);
        int[] alelos_hijo1 = new int[TAM_GEN];
        int[] alelos_hijo2 = new int[TAM_GEN];
        for (int i = 0; i < TAM_GEN; i++) {
            alelos_hijo1[i] = i; // Para comprobar que no se repiten al copiar al hijo
            alelos_hijo2[i] = i;
        }

        // Copia en los hijos los alelos comprendidos entre posA y posB
        for (int i = posA; i <= posB; i++) {
            // Primer hijo
            hijo1.genotipo[i] = poblacion[posPadre].genotipo[i];
            alelos_hijo1[poblacion[posPadre].genotipo[i]] = ESTA;
            num_h1++;
            // Segundo hijo
            hijo2.genotipo[i] = poblacion[posMadre].genotipo[i];
            alelos_hijo2[poblacion[posMadre].genotipo[i]] = ESTA;
            num_h2++;
        }

        // it: Iterador
        // k: Posición donde insertar en el hijo1
        // z: Posición donde insertar en el hijo2
        int it, k, z;

        // Si el segmento está al final
        if (posB + 1 == TAM_GEN) {
            it = k = z = 0;
        } else {
            it = k = z = posB + 1;
        }

        if (it != posA) {
            do {
                // Primer hijo
                if (alelos_hijo1[poblacion[posMadre].genotipo[it]] != ESTA) {
                    hijo1.genotipo[k] = poblacion[posMadre].genotipo[it];
                    alelos_hijo1[poblacion[posMadre].genotipo[it]] = ESTA;
                    k++;
                    num_h1++;
                }
                // Segundo hijo
                if (alelos_hijo2[poblacion[posPadre].genotipo[it]] != ESTA) {
                    hijo2.genotipo[z] = poblacion[posPadre].genotipo[it];
                    alelos_hijo2[poblacion[posPadre].genotipo[it]] = ESTA;
                    z++;
                    num_h2++;
                }
                it++;
                if (k == TAM_GEN) k = 0;
                if (z == TAM_GEN) z = 0;
                if (it == TAM_GEN) it = 0;
            } while (num_h1 < TAM_GEN || num_h2 < TAM_GEN);
        }
    } // cruce_orden()

    /**
     * @brief Operador de cruce (cruce PMX)
     * @post Cruzar los dos padres seleccionados previamente y calcular dos hijos
     */
    private void cruce_PMX() {
        int posA = random.nextInt(TAM_GEN);
        int posB = random.nextInt(TAM_GEN);
        int c;
        int num_h1 = 0, num_h2 = 0;

        // posA tiene que ser menor que posB
        if (posA > posB) {
            c = posA;
            posA = posB;
            posB = c;
        }
        // Creación e inicialización de los hijos
        hijo1 = new Individuo(TAM_GEN);
        hijo2 = new Individuo(TAM_GEN);
        int[] alelos_hijo1 = new int[TAM_GEN];
        int[] alelos_hijo2 = new int[TAM_GEN];
        for (int i = 0; i < TAM_GEN; i++) {
            hijo1.genotipo[i] = LIBRE; // Para indicar una posición libre en el hijo
            hijo2.genotipo[i] = LIBRE;
            alelos_hijo1[i] = i; // Para comprobar que no se repiten al copiar al hijo
            alelos_hijo2[i] = i;
        }

        // Copia en los hijos los alelos comprendidos entre posA y posB
        for (int i = posA; i <= posB; i++) {
            // Primer hijo
            hijo1.genotipo[i] = poblacion[posPadre].genotipo[i];
            alelos_hijo1[poblacion[posPadre].genotipo[i]] = ESTA;
            num_h1++;
            // Segundo hijo
            hijo2.genotipo[i] = poblacion[posMadre].genotipo[i];
            alelos_hijo2[poblacion[posMadre].genotipo[i]] = ESTA;
            num_h2++;
        }

        // Iterador
        int it = posA;
        int it2; // Usado para pivotar
        boolean parar;
        do {
            it2 = it;
            parar = false;

            // Comprobación si el valor (de la madre) no esta en el hijo1
            if (alelos_hijo1[poblacion[posMadre].genotipo[it]] != ESTA) {
                // Mientras no se encuentre posición vacía en el hijo (con -2)
                while (!parar) {
                    if (hijo1.genotipo[it2] == LIBRE) {
                        hijo1.genotipo[it2] = poblacion[posMadre].genotipo[it];
                        alelos_hijo1[poblacion[posMadre].genotipo[it]] = ESTA;
                        num_h1++;
                        parar = true;
                    } else {
                        it2 = poblacion[posMadre].busca_valor(hijo1.genotipo[it2]);
                    }
                } // while
            } // if

            it2 = it;
            parar = false;
            // Comprobación si el valor (del padre) no esta en el hijo2
            if (alelos_hijo2[poblacion[posPadre].genotipo[it]] != ESTA) {
                // Mientras no se encuentre posición vacía en el hijo (con -2)
                while (!parar) {
                    if (hijo2.genotipo[it2] == LIBRE) {
                        hijo2.genotipo[it2] = poblacion[posPadre].genotipo[it];
                        alelos_hijo2[poblacion[posPadre].genotipo[it]] = ESTA;
                        num_h2++;
                        parar = true;
                    } else {
                        it2 = poblacion[posPadre].busca_valor(hijo2.genotipo[it2]);
                    }
                } // while
            } // if

            it++;
            if (it == TAM_GEN) it = 0;
        } while (num_h1 < TAM_GEN || num_h2 < TAM_GEN);
    } // cruce_PMX()

    /**
     * @brief Operador de mutación
     * @post Muta cada alelo de cada hijo de acuerdo a una probabilidad
     */
    private void mutacion() {
        double aleatorio; // Para saber si mutar o no el alelo
        int pos_aleatoria;
        // Probabilidad de mutación
        double P_mutacion = 0.001 * TAM_GEN;
        int aux; // Variable auxiliar para intercambio

        for (int i = 0; i < TAM_GEN; i++) {
            aleatorio = random.nextDouble();
            if (aleatorio <= P_mutacion) {
                pos_aleatoria = random.nextInt(TAM_GEN);
                aux = hijo1.genotipo[i];
                hijo1.genotipo[i] = hijo1.genotipo[pos_aleatoria];
                hijo1.genotipo[pos_aleatoria] = aux;
            }
            aleatorio = random.nextDouble();
            if (aleatorio <= P_mutacion) {
                pos_aleatoria = random.nextInt(TAM_GEN);
                aux = hijo2.genotipo[i];
                hijo2.genotipo[i] = hijo2.genotipo[pos_aleatoria];
                hijo2.genotipo[pos_aleatoria] = aux;
            }
        } // for i
    } // mutacion()

    /**
     * @brief Reemplazo de individuos
     * @post Reemplaza los dos nuevos individuos sólo si son mejores que los dos peores de la población
     */
    private void reemplazamiento() {
        int pos_FIRST = 0;
        int pos_SECOND = 0;

        // Búsqueda de los peores dos individuos de la población
        for (int i = 1; i < TAM_POBLACION; i++) {
            if (poblacion[pos_FIRST].fitness < poblacion[i].fitness) {
                pos_SECOND = pos_FIRST;
                pos_FIRST = i;
            }
        }
        boolean first_usado = false;
        // Cambiamos el peor por el hijo sólo si el hijo es mejor
        if (hijo1.fitness < poblacion[pos_FIRST].fitness) {
            // Log      -inicio-
            Main.informe_E += +poblacion[pos_FIRST].id;
            Main.informe_E += " ; ";
            for (int i = 0; i < fabrica.num_unidades; i++)
                Main.informe_E = Main.informe_E + " " + poblacion[pos_FIRST].genotipo[i];
            Main.informe_E += " ; " + poblacion[pos_FIRST].fitness;
            Main.informe_E += " ; " + poblacion[pos_FIRST].generacion;
            Main.informe_E += " ;<->; ";
            // Log      -fin-
            poblacion[pos_FIRST].copiar_individuo(hijo1);
            first_usado = true;
            // Log      -inicio-
            Main.informe_E += +poblacion[pos_FIRST].id;
            Main.informe_E += " ; ";
            for (int i = 0; i < fabrica.num_unidades; i++)
                Main.informe_E = Main.informe_E + " " + poblacion[pos_FIRST].genotipo[i];
            Main.informe_E += " ; " + poblacion[pos_FIRST].fitness;
            Main.informe_E += " ; " + poblacion[pos_FIRST].generacion;
            Main.informe_E += "\n";
            // Log      -fin-
        } else if (hijo1.fitness < poblacion[pos_SECOND].fitness) {
            // Log      -inicio-
            Main.informe_E += +poblacion[pos_SECOND].id;
            Main.informe_E += " ; ";
            for (int i = 0; i < fabrica.num_unidades; i++)
                Main.informe_E = Main.informe_E + " " + poblacion[pos_SECOND].genotipo[i];
            Main.informe_E += " ; " + poblacion[pos_SECOND].fitness;
            Main.informe_E += " ; " + poblacion[pos_SECOND].generacion;
            Main.informe_E += " ;<->; ";
            // Log      -fin-
            poblacion[pos_SECOND].copiar_individuo(hijo1);
            first_usado = true;
            // Log      -inicio-
            Main.informe_E += +poblacion[pos_SECOND].id;
            Main.informe_E += " ; ";
            for (int i = 0; i < fabrica.num_unidades; i++)
                Main.informe_E = Main.informe_E + " " + poblacion[pos_SECOND].genotipo[i];
            Main.informe_E += " ; " + poblacion[pos_SECOND].fitness;
            Main.informe_E += " ; " + poblacion[pos_SECOND].generacion;
            Main.informe_E += "\n";
            // Log      -fin-
        }
        if (!first_usado) {
            if (hijo2.fitness < poblacion[pos_FIRST].fitness) {
                // Log      -inicio-
                Main.informe_E += +poblacion[pos_FIRST].id;
                Main.informe_E += " ; ";
                for (int i = 0; i < fabrica.num_unidades; i++)
                    Main.informe_E = Main.informe_E + " " + poblacion[pos_FIRST].genotipo[i];
                Main.informe_E += " ; " + poblacion[pos_FIRST].fitness;
                Main.informe_E += " ; " + poblacion[pos_FIRST].generacion;
                Main.informe_E += " ;<->; ";
                // Log      -fin-
                poblacion[pos_FIRST].copiar_individuo(hijo2);
                // Log      -inicio-
                Main.informe_E += +poblacion[pos_FIRST].id;
                Main.informe_E += " ; ";
                for (int i = 0; i < fabrica.num_unidades; i++)
                    Main.informe_E = Main.informe_E + " " + poblacion[pos_FIRST].genotipo[i];
                Main.informe_E += " ; " + poblacion[pos_FIRST].fitness;
                Main.informe_E += " ; " + poblacion[pos_FIRST].generacion;
                Main.informe_E += "\n";
                // Log      -fin-
            }
        } else if (hijo2.fitness < poblacion[pos_SECOND].fitness) {
            // Log      -inicio-
            Main.informe_E += +poblacion[pos_SECOND].id;
            Main.informe_E += " ; ";
            for (int i = 0; i < fabrica.num_unidades; i++)
                Main.informe_E = Main.informe_E + " " + poblacion[pos_SECOND].genotipo[i];
            Main.informe_E += " ; " + poblacion[pos_SECOND].fitness;
            Main.informe_E += " ; " + poblacion[pos_SECOND].generacion;
            Main.informe_E += " ;<->; ";
            // Log      -fin-
            poblacion[pos_SECOND].copiar_individuo(hijo2);
            // Log      -inicio-
            Main.informe_E += +poblacion[pos_SECOND].id;
            Main.informe_E += " ; ";
            for (int i = 0; i < fabrica.num_unidades; i++)
                Main.informe_E = Main.informe_E + " " + poblacion[pos_SECOND].genotipo[i];
            Main.informe_E += " ; " + poblacion[pos_SECOND].fitness;
            Main.informe_E += " ; " + poblacion[pos_SECOND].generacion;
            Main.informe_E += "\n";
            // Log      -fin-
        }
    } // reemplazamiento()

    /**
     * @brief Evaluación de los dos nuevos individuos
     */
    private void evaluacion() {
        hijo1.calcular_fitness(fabrica);
        hijo2.calcular_fitness(fabrica);
    } // evaluacion()

    /**
     * @param fabrica Fabrica en la que aplicar el algoritmo
     * @brief Algoritmo principal de la clase
     */
    void algoritmo_principal(Fabrica fabrica) {
        // Semilla inicializada
        random = new Random(Main.semilla);
        Main.evaluaciones = 0;
        Main.generaciones = 0;
        Individuo.NUM = 0;

        this.fabrica = fabrica;
        TAM_GEN = fabrica.num_unidades;

        // Creación de la matriz con los 50 individuos
        poblacion = new Individuo[TAM_POBLACION];
        // Inicialización de las poblaciones
        inicializar_poblacion();

        // Cálculo del fitness de la población
        calcular_fitness_inicial();

        do {
            seleccion();
            if (Main.Opcion_local.equals(Main.CRUCE_ORDEN) || Main.Opcion_local.equals(Main.ALL)) {
                cruce_orden();
            }
            if (Main.Opcion_local.equals(Main.CRUCE_PMX) || Main.Opcion_local.equals(Main.ALL)) {
                cruce_PMX();
            }

            mutacion();
            evaluacion();
            reemplazamiento();
            Main.generaciones++;
        } while (Main.evaluaciones < 50000);
    } // algoritmo_principal()
} // class Gen_estacionario
