/**
 * @author Manuel Alférez Ruiz
 * @note Metaheurística Práctica 1
 */

package meta;

/**
 * @brief Clase que realiza la suma de las filas de las matrices:
 * - Flujo de Piezas
 * - Distancias
 * de una Fabrica y las almacena
 */
public class Suma {

    // Vector sumaPiezas: Cada posición representa la suma de la fila de la matriz de flujo de Piezas
    // Vector sumaDistancias: Cada posición representa la suma de la fila de la matriz de distancias
    // tama: Almacena el tamaño que tienen los dos vectores

    protected int[] sumaPiezas;
    protected int[] sumaDistancias;
    int tama;

    /**
     * @brief Constructor parametrizado
     */
    public Suma(Fabrica fabrica) {

        tama = fabrica.num_unidades;

        // Creación de vectores
        sumaPiezas = new int[tama];
        sumaDistancias = new int[tama];

        // Variables
        int sumaFilaPiezas;
        int sumaFilaDistancias;

        for (int i = 0; i < tama; i++) {
            sumaFilaPiezas = 0;
            sumaFilaDistancias = 0;
            for (int j = 0; j < tama; j++) {
                sumaFilaPiezas += fabrica.piezas[i][j];
                sumaFilaDistancias += fabrica.distancias[i][j];
            }
            sumaPiezas[i] = sumaFilaPiezas;
            sumaDistancias[i] = sumaFilaDistancias;
        }
    } // constructor parametrizado

    /**
     * @brief Constructor de copia
     */
    public Suma(Suma suma) {

        tama = suma.tama;

        // Creación de vectores
        sumaPiezas = new int[tama];
        sumaDistancias = new int[tama];

        // Copia de los vectores
        for (int i = 0; i < tama; i++) {
            sumaPiezas[i] = suma.sumaPiezas[i];
            sumaDistancias[i] = suma.sumaDistancias[i];
        }

    } // constructor de copia

} // class Suma
