/**
 * @author Manuel Alférez Ruiz
 * @note Metaheurística Práctica 1
 */

package meta;

// Dependencies

import java.util.Random;

/**
 * @brief Clase que maneja la resolución del problema mediante un enfoque de Búsqueda Local del Primer Mejor
 */
public class BL_memeticos {

    // mascara_DLB: contiene 0 o 1 y marca las zonas del entorno que se pueden visitar (0 si, 1 no)
    // no_Mejora: representa un estado en el que no hay ningún vecino mejor que la situación actual
    // coste_vecino y coste_Sact: costes del vecino y de la situación actual
    // vecino: vecino generado generado

    private int[] mascara_DLB;
    private boolean no_Mejora;
    private int iteraciones;
    private int coste_vecino;
    private int coste_vecino_aux;
    public int coste_Sact;
    private int[] vecino;
    private Fabrica fabrica;
    Random random;

//    /**
//     * @param permutacion Permutación a la que calcular el peso
//     * @return El peso de la permutación
//     * @brief Calcula el peso de una permutación
//     */
//    private int coste(int[] permutacion) {
//        int peso = 0;
//        for (int i = 0; i < fabrica.num_unidades; i++)
//            for (int j = 0; j < fabrica.num_unidades; j++)
//                if (i != j)
//                    peso += fabrica.piezas[i][j] * fabrica.distancias[permutacion[i]][permutacion[j]];
//
//        return peso;
//    } // coste() -> Complejidad en tiempo= n²

    /**
     * @param fabrica Fabrica en la que aplicar el algoritmo
     * @param r       Posición de intercambio
     * @param s       Posición de intercambio
     * @brief Calcula el fitness del individuo después de realizar un intercambio
     * @note Se realiza el intercambio de la posición dentro del algoritmo
     */
    protected int coste_parcial(int[] permutacion, int r, int s) {
        // Cálculo del fitness parcial con las unidades implicadas antes de intercambiarlas
        int fitness_parcialA = 0;
        for (int i = 0; i < fabrica.num_unidades; i++) {
            if (i != r) {
                fitness_parcialA += fabrica.piezas[r][i] * fabrica.distancias[permutacion[r]][permutacion[i]];
                fitness_parcialA += fabrica.piezas[i][r] * fabrica.distancias[permutacion[i]][permutacion[r]];
            }
            if (i != s) {
                fitness_parcialA += fabrica.piezas[s][i] * fabrica.distancias[permutacion[s]][permutacion[i]];
                fitness_parcialA += fabrica.piezas[i][s] * fabrica.distancias[permutacion[i]][permutacion[s]];
            }
        }
        int c = permutacion[r];
        permutacion[r] = permutacion[s];
        permutacion[s] = c;

        // Cálculo del fitness parcial con las unidades implicadas después de intercambiarlas
        int fitness_parcialB = 0;
        for (int i = 0; i < fabrica.num_unidades; i++) {
            if (i != r) {
                fitness_parcialB += fabrica.piezas[r][i] * fabrica.distancias[permutacion[r]][permutacion[i]];
                fitness_parcialB += fabrica.piezas[i][r] * fabrica.distancias[permutacion[i]][permutacion[r]];
            }
            if (i != s) {
                fitness_parcialB += fabrica.piezas[s][i] * fabrica.distancias[permutacion[s]][permutacion[i]];
                fitness_parcialB += fabrica.piezas[i][s] * fabrica.distancias[permutacion[i]][permutacion[s]];
            }
        }

        // Cálculo del coste de la población al realizar el intercambio r, s
        return coste_vecino - (fitness_parcialA - fitness_parcialB);
    } // coste_parcial() -> Complejidad = n-1

    /**
     * @param Sact Situación actual
     * @param r    Posición a intercambiar
     * @param s    Posición a intercambiar
     * @return Devuelve una permutación que contiene el intercambio
     * @brief Operador de intercambio
     */
    private int[] operadorInt(int[] Sact, int r, int s) {
        int c = Sact[r];
        Sact[r] = Sact[s];
        Sact[s] = c;

        return Sact;
    } // operadorInt() -> Complejidad en tiempo= 1

    /**
     * @param a    El vector origen
     * @param b    El vector destino
     * @param tama El tamaño de los vectores
     * @brief Realiza una copia de un vector
     */
    private void copia(int[] a, int[] b, int tama) {
        for (int i = 0; i < tama; i++)
            b[i] = a[i];
    } // copia() -> Complejidad en tiempo= n

    /**
     * @param Sact Situación actual
     * @return Devuelve el primer vecino mejor
     * @brief Realiza la búsqueda del primer vecino mejor:
     * Se realiza una permutación sobre la Sact y se elige la primera que se encuentre que sea mejor atendiendo al
     * siguiente proceder:
     * - Si la máscara se encuentra a 1 en dicha posición no se tiene en cuenta una permutación con dicho valor
     * ya que en una iteración anterior se hicieron todas las iteraciones y no hubo ningún vecino prometedor (mejor
     * que la Sact).
     * - Las permutaciones con la misma posición se obvian (i=j)
     * - A continuación se realiza la permutación correspondiente a la posición que haya pasado todos los anteriores
     * filtros
     * - Si el coste del vecino generado es mejor entonces se para la ejecución de la función y se devuelve dicha permutación
     */
    private int[] genera_vecino(int[] Sact) {
        // Marcará cuando el bucle principal de abajo tenga que detenerse
        boolean flag_parada = false;
        coste_vecino_aux=coste_vecino;

        for (int i = 0; i < Sact.length; i++) {
            if (mascara_DLB[i] != 1) {
                for (int j = 0; j < Sact.length; j++) {
                    if (mascara_DLB[j] != 1) {
                        if (i != j) {
//                            operadorInt(vecino, i, j);
//                            coste_vecino = coste(vecino);
                            coste_vecino_aux = coste_parcial(vecino, i, j);
                            if ((coste_vecino_aux - coste_Sact) < 0) {
                                iteraciones++;
                                coste_vecino=coste_vecino_aux;
                                if (iteraciones == Main.val_ite) {
                                    flag_parada = true;
                                    break;
                                }
                                flag_parada = true;
                                mascara_DLB[i] = 0;
                                mascara_DLB[j] = 0;
                                break;
                            } else {
                                operadorInt(vecino, i, j);
                                coste_vecino_aux=coste_vecino;
                            }
                        }
                    } // if mascara j

                } // for j

            } // if mascara i

            // Se ha generado el espacio al completo
            if (i == Sact.length - 1) {
                no_Mejora = true;
                mascara_DLB[i] = 1;
            }

            if (flag_parada)
                break;

        } // for i

        return vecino;
    } // genera_vecino()

    /**
     * @param fabrica Fabrica en la que aplicar el algoritmo
     * @return Devuelve la permutación solución del problema
     * @brief Algoritmo principal de búsqueda del primer mejor
     */
    public int[] busquedaLocalPrimerMejor(Fabrica fabrica, Individuo individuo) {

        random = new Random(Main.semilla);

        this.fabrica = fabrica;

        // Creación de la máscara
        mascara_DLB = new int[fabrica.num_unidades];

        // Inicialización de la máscara
        for (int i = 0; i < fabrica.num_unidades; i++)
            mascara_DLB[i] = 0;

        // Contador del número de iteraciones
        iteraciones = 0;

        no_Mejora = false;

        // Bucle del algoritmo
        int[] Sact = new int[individuo.tam_gen];
        copia(individuo.genotipo, Sact, individuo.tam_gen);
        this.vecino = new int[Sact.length];
        copia(Sact, vecino, Sact.length);

        // Calculamos el coste de la Sact
        coste_Sact = individuo.fitness;
        coste_vecino= coste_Sact;

        do {
            vecino = genera_vecino(Sact);
            Sact = vecino;
            coste_Sact = coste_vecino;
        } while (iteraciones < Main.val_ite && !no_Mejora);

        return Sact;
    } // busquedaLocalPrimerMejor()

} // class BL_memeticos
