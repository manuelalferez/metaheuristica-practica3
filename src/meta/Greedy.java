/**
 * @author Manuel Alférez Ruiz
 * @note Metaheurística Práctica 1
 */

package meta;

/**
 * @brief Clase que maneja la resolución del problema mediante un enfoque Greedy
 */
public class Greedy {

    /**
     * @param suma Clase que contiene los vectores suma de las filas de las matrices Piezas y Distancias
     * @return Devuelve un vector solución, que representa la solución del problema
     * @brief Algoritmo Greedy:
     * <p>
     * Planteamiento del problema. Dado un vector de sumaPiezas y otro sumaDistancias (los cuales contiende la suma
     * de las filas de las matrices de Piezas y Distancias respectivamente), debemos de obtener una asignación de
     * unidades de producción de forma que la unidad que tenga mayor flujo de piezas esté en el lugar de la unidad
     * que tiene menor distancia con respecto a las demás unidades de producción.
     * <p>
     * Solución dada por el Algoritmo greedy:
     * <p>
     * Se escoge una posición del vector de sumaPiezas de forma que sea el mayor valor disponible, después se escoge una
     * posición del vector sumaDistancias de forma que sea el menor valor disponible. A continuación se anota el
     * intercambio de la siguiente manera:
     * - Dentro del vector solución, en la posición donde se encuentra el menor valor disponible del vector sumaDistancias
     * se introduce como valor la posición donde se encuentra el mayor valor disponible dentro del vector sumaPiezas.
     * A continuación, procedemos a eliminar las unidades de producción que hemos intercambiado para que quede reflejado
     * en el vector de sumaPiezas y sumaDistancias, mediante el siguiente proceder:
     * - En la posición donde se ha encontrado el mayor valor disponible del vector de sumaPiezas se introduce el valor
     * -1.
     * - En la posición do  nde se ha encontrado el menor valor disponible del vector de sumaDistancias se introduce el valor
     * -1.
     * De esta manera se descartan contemplar estas posiciones en futuras búsquedas.
     * Para finalizar faltaría ir decrementando en un el tamaño del vector para indicar la parada del proceso.
     */
    public int[] algoritmoGreedy(Suma suma) {

        // Vector solución
        int[] sol = new int[suma.tama];

        int posP = 0;
        int posD = 0;

        int mayorP, menorD;
        int tamanio = suma.tama;

        // Copia para no borrar la Suma original
        Suma sumaCopia = new Suma(suma);

        do {
            mayorP = Integer.MIN_VALUE;
            menorD = Integer.MAX_VALUE;
            for (int i = 0; i < suma.tama; i++) {
                if (sumaCopia.sumaPiezas[i] != -1) {
                    if (sumaCopia.sumaPiezas[i] > mayorP) {
                        posP = i;
                        mayorP = sumaCopia.sumaPiezas[i];
                    }
                }
                if (sumaCopia.sumaDistancias[i] != -1) {
                    if (sumaCopia.sumaDistancias[i] < menorD) {
                        posD = i;
                        menorD = sumaCopia.sumaDistancias[i];
                    }
                }
            }

            // Añadimos solución encontrada
            sol[posP] = posD;

            // Quitamos elementos de los vectores de datos
            sumaCopia.sumaPiezas[posP] = -1;
            sumaCopia.sumaDistancias[posD] = -1;

            tamanio--;

        } while (tamanio > 0);

        return sol;
    } // algoritmoGreedy()

} // class Greedy
